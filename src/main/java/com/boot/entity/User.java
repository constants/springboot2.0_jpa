package com.boot.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "sys_user")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false,length = 20,unique = true)
    private String username;
    @Column(nullable = false,length = 32)
    private String password;

}
