package com.boot.onetomany.services.impl;

import com.boot.onetomany.dao.JpaStudentDao;
import com.boot.onetomany.entity.Student;
import com.boot.onetomany.services.StudentServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class StudentServicesImpl implements StudentServices {

    @Autowired
    private JpaStudentDao jpaStudentDao;

    @Override
    public Student save(Student student) {
        return jpaStudentDao.save(student);
    }

    @Transactional
    @Override
    public Student query(Integer id) {
        Optional<Student> optional = jpaStudentDao.findById(id);
        Student student = optional.get();
        // 因为在Student实体中，OneToMany　默认是懒加载，而懒加载只有使用到属性的时候，才会调用
        System.out.println(student);
        return optional.get();
    }
}
