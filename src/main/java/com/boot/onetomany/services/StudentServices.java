package com.boot.onetomany.services;

import com.boot.onetomany.entity.Student;

public interface StudentServices {

    Student save(Student student);

    Student query(Integer id);
}
