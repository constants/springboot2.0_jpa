package com.boot.onetomany.dao;

import com.boot.onetomany.entity.Books;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaBooksDao extends JpaRepository<Books,Integer> {
}
