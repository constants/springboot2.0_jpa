package com.boot.onetomany.dao;

import com.boot.onetomany.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaStudentDao extends JpaRepository<Student,Integer> {
}
