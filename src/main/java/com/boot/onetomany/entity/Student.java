package com.boot.onetomany.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "one_student")
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false,length = 20)
    private String studName;
    @Column(nullable = false,length = 30)
    private String className;
    // REFRESH 级联刷新
    // PERSIST 级联保存
    // MERGE   级联更新
    // REMOVE  级联删除 <先删除子,后删除主>
    // ALL 指上面四种
    // fetch = FetchType.LAZY  延迟加载 oneToMany 后面是Many的默认都是LAZY,是one的默认都是EAGER
    // fetch = FetchType.EAGER 立即加载
    // mappedBy 关系被维护方,维护方的属性名称
    @OneToMany(cascade = {CascadeType.PERSIST},mappedBy = "student")
    private Set<Books> books = new HashSet<>();

    // 级联保存
    public void addBooks(Books book){
        book.setStudent(this);
        this.books.add(book);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", studName='" + studName + '\'' +
                ", className='" + className + '\'' +
                ", books=" + books +
                '}';
    }

    public Student() {
        super();
    }

    public Student(String studName, String className, Set<Books> books) {
        this.studName = studName;
        this.className = className;
        this.books = books;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudName() {
        return studName;
    }

    public void setStudName(String studName) {
        this.studName = studName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Set<Books> getBooks() {
        return books;
    }

    public void setBooks(Set<Books> books) {
        this.books = books;
    }
}
