package com.boot.onetomany.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "one_books")
public class Books implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false,length = 50)
    private String bookName;
    @Column(nullable = false)
    private String auother;

    // REFRESH 级联刷新
    // optional = true 表示对象值没有,可以为null,默认true
    @ManyToOne(optional = false )
    @JoinColumn(name = "stu_id")
    private Student student;

    @Override
    public String toString() {
        return "Books{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", auother='" + auother + '\'' +
                '}';
    }

    public Books() {
        super();
    }


    public Books(String bookName, String auother, Student student) {
        this.bookName = bookName;
        this.auother = auother;
        this.student = student;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuother() {
        return auother;
    }

    public void setAuother(String auother) {
        this.auother = auother;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
