package com.boot.manytomany.dao;

import com.boot.manytomany.entity.ClassName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaClassNameDao extends JpaRepository<ClassName,Integer> {
}
