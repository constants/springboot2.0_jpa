package com.boot.manytomany.dao;

import com.boot.manytomany.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaStudentManyDao extends JpaRepository<Student,Integer> {
}
