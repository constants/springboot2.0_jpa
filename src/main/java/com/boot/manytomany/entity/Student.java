package com.boot.manytomany.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "many_student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false,length = 30)
    private String name;    // 姓名
    @Column(nullable = false,length = 15)
    private String schNumber; // 学生编号
    @Column(length = 50)
    private String address;// 公寓地址

    //级联刷新  维护方
    @ManyToMany(cascade = {CascadeType.REFRESH})
    // joinTable > name 是中间表名称,inverseJoinColumns是被维护方   joinColumns是维护方
    @JoinTable(name = "student_class",inverseJoinColumns = @JoinColumn(name = "class_id")
    ,joinColumns = @JoinColumn(name = "student_id"))
    private Set<ClassName> classNames = new HashSet<>();

    //添加
    public void addClassName(ClassName className){
        this.classNames.add(className);
    }
    // 删除
    public void delClassName(ClassName className){
        if(this.classNames.contains(className)){
            this.classNames.add(className);
        }
    }

    public Student(String name, String schNumber, String address) {
        this.name = name;
        this.schNumber = schNumber;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchNumber() {
        return schNumber;
    }

    public void setSchNumber(String schNumber) {
        this.schNumber = schNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<ClassName> getClassNames() {
        return classNames;
    }

    public void setClassNames(Set<ClassName> classNames) {
        this.classNames = classNames;
    }

    public Student() {
        super();
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", schNumber='" + schNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
