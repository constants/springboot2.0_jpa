package com.boot.manytomany.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "many_classname")
public class ClassName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false,length = 50)
    private String className;   // 班级名称
    @Column(nullable = false,length = 10)
    private String classNumber; // 班级编号

    // 级联刷新,学生维护方
    @ManyToMany(cascade = {CascadeType.REFRESH},mappedBy = "classNames")
    private Set<Student> students = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassName className = (ClassName) o;
        return Objects.equals(id, className.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public ClassName(String className, String classNumber) {
        this.className = className;
        this.classNumber = classNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public ClassName() {
        super();
    }

    @Override
    public String toString() {
        return "ClassName{" +
                "id=:" + id +
                ", className='" + className + '\'' +
                ", classNumber='" + classNumber + '\'' +
                '}';
    }
}
