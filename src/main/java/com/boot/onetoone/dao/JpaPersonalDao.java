package com.boot.onetoone.dao;

import com.boot.onetoone.entity.Personal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaPersonalDao extends JpaRepository<Personal,Integer> {
}
