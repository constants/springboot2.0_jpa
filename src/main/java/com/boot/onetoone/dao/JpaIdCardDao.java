package com.boot.onetoone.dao;

import com.boot.onetoone.entity.IdCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaIdCardDao extends JpaRepository<IdCard,Integer> {
}
