package com.boot.onetoone.entity;


import javax.persistence.*;

/**
 *  案例: 用户和身份证,一对一关系
 */
@Entity
@Table(name = "one_personal")
public class Personal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false,length = 30)
    private String username;
    @Column(nullable = false,length = 3)
    private Integer age;
    @Column(nullable = false,length = 2)
    private String sex;

    @OneToOne(optional = false,cascade = {CascadeType.ALL})
    @JoinColumn(name = "idcard_id")
    private IdCard idCard;

    public Personal(String username, Integer age, String sex) {
        this.username = username;
        this.age = age;
        this.sex = sex;
    }

    public Personal() {
        super();
    }

    public IdCard getIdCard() {
        return idCard;
    }

    public void setIdCard(IdCard idCard) {
        this.idCard = idCard;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Personal{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
