package com.boot.onetoone.entity;

import javax.persistence.*;

@Entity
@Table(name = "one_idcard")
public class IdCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false,length = 18,unique = true)
    private String idNumber;
    @Column(nullable = false,length = 50)
    private String address;

    @OneToOne(mappedBy = "idCard",cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH})
    private Personal personal;

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public IdCard() {
        super();
    }
    public IdCard(String idNumber, String address) {
        this.idNumber = idNumber;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "IdCard{" +
                "id=" + id +
                ", idNumber='" + idNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
