package com.boot;

import com.boot.entity.User;
import com.boot.manytomany.dao.JpaClassNameDao;
import com.boot.manytomany.dao.JpaStudentManyDao;
import com.boot.manytomany.entity.ClassName;
import com.boot.onetomany.entity.Books;
import com.boot.onetomany.entity.Student;
import com.boot.onetomany.services.StudentServices;
import com.boot.onetoone.dao.JpaIdCardDao;
import com.boot.onetoone.dao.JpaPersonalDao;
import com.boot.onetoone.entity.IdCard;
import com.boot.onetoone.entity.Personal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Test
    public void contextLoads() {
        String sql = "select * from sys_user";
        List<User> listUser = jdbcTemplate.query(sql, new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs, int i) throws SQLException {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                return user;
            }
        });

        for (User user : listUser) {
            System.out.println(user.toString());
        }
    }

    // 测试一对多关系
    @Resource
    private StudentServices studentServices;

    @Test
    public void testOntToMany(){
        Student student = new Student();
        student.setClassName("第一季度");
        student.setStudName("小当家");
        //Student dbStudent = studentServices.save(student);

        Books books1 = new Books();
        books1.setAuother("金庸");
        books1.setBookName("天龙八部");
        //books1.setStudent(dbStudent);
        //booksDao.save(books1);

        Books books2 = new Books();
        books2.setAuother("金庸");
        books2.setBookName("笑傲江湖");
        //books2.setStudent(dbStudent);
        //booksDao.save(books2);

        student.addBooks(books1);
        student.addBooks(books2);

        studentServices.save(student);
    }

    @Test
    public void queryStudent(){
        Student student = studentServices.query(2);
        System.out.println(student);
    }

    // 测试 一对一关系
    @Autowired
    private JpaPersonalDao jpaPersonalDao;
    @Autowired
    private JpaIdCardDao jpaIdCardDao;

    @Test
    public void oneToOne(){
        IdCard idCard = new IdCard();
        idCard.setAddress("中国");
        idCard.setIdNumber("4127251992433");

        Personal personal = new Personal();
        personal.setAge(18);
        personal.setSex("男");
        personal.setUsername("朱");
        personal.setIdCard(idCard);
        // 保存
        jpaPersonalDao.save(personal);
        // 查询
        Optional<Personal> optional = jpaPersonalDao.findById(1);
        Personal dbPersonal = optional.get();
        System.out.println(dbPersonal);
        System.out.println(dbPersonal.getIdCard());
    }

    // 多对多测试
    @Autowired
    private JpaClassNameDao jpaClassNameDao;
    @Autowired
    private JpaStudentManyDao jpaStudentManyDao;

    /**
     *  测试多对多关系,生成表
     */
    @Test
    public void createManyToManyTable(){
        new com.boot.manytomany.entity.Student();
        new ClassName();
    }

    /**
     * 测试多对多关系，保存
     */
    @Test
    public void manyToMany(){
        ClassName className = new ClassName();
        // 班级1
        className.setClassName("艺术传媒");
        className.setClassNumber("1001-1-1");
        ClassName dbClassName1 = jpaClassNameDao.save(className);
        // 班级2
        ClassName className2 = new ClassName();
        className2.setClassName("信息工程");
        className2.setClassNumber("1002-1-1");
        ClassName dbClassName2 = jpaClassNameDao.save(className2);

        com.boot.manytomany.entity.Student student = new com.boot.manytomany.entity.Student();
        // 测试添加  学生1
        student.setAddress("1001-1-1");
        student.setName("张三");
        student.setSchNumber("201810010001");
        // 学生和班级
        student.addClassName(dbClassName1);
        student.addClassName(dbClassName2);

        jpaStudentManyDao.save(student);

    }

    /**
     * 删除学生,中间表也会删除
     */
    @Test
    public void delManyToManyStudent(){
        jpaStudentManyDao.delete(jpaStudentManyDao.getOne(1));
    }

    /**
     * 删除学生和班级的中间表
     */
    @Test
    public void delManyTomanyStudentClass(){

    }
}
